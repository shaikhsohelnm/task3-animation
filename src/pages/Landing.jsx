import React, { useState, useEffect, useRef } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import styled from "@emotion/styled";
import fitneddimg from "../images/measure-total-body-weight-through-fitness-app.png";
import fitneddimgSecond from "../images/nasa-fitness-tracking-mobile-app.png";
import pizza1 from "../images/Secondcarousel1.png";
import pizza2 from "../images/Secondcarousel2.png";
import pizza3 from "../images/Secondcarousel3.png";
import pizza4 from "../images/Secondcarousel4.png";
import pizza5 from "../images/Secondcarousel5.png";
import pizza6 from "../images/Secondcarousel6.png";
import pizza7 from "../images/Secondcarousel7.png";
import thirdcarouseone from "../images/Thirdcarousel1.png";
import thirdcarousetwo from "../images/Thirdcarousel2.png";
import thirdcarousethree from "../images/Thirdcarousel3.png";
import thirdcarousefour from "../images/Thirdcarousel4.png";
import thirdcarousefive from "../images/Thirdcarousel5.png";
import fourthcarouseone from "../images/fourthcarousel1.png";
import fifthcarouseone from "../images/fifthcarousel1.png";
import fifthcarousetwo from "../images/fifthcarousel2.png";
import fifthcarousethree from "../images/fifthcarousel3.png";
import sixthcarouseone from "../images/Sixthcaousel1.png";
import sixthcarousetwo from "../images/Sixthcaousel2.png";
import seventhcarouseone from "../images/Seventhcarousel1.png";
import seventhcarousetwo from "../images/Seventhcarousel2.png";
import seventhcarousethree from "../images/Seventhcarousel3.png";

import { gsap, Power3 } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
gsap.registerPlugin(ScrollTrigger);

const WrapperStyled = styled.div`
  position: relative;
  a {
    text-decoration: none;
    color: #fff;
  }
  margin: 0;
  padding: 0;

  .carouselwrapper {
    padding: 0;
    margin: 0;
    border: 0;
    outline: 0;
    width: 100dvw;
    height: 100dvh;
    display: grid;
    grid-template-columns: 1fr;
    grid-template-rows: 1fr 1fr;
    .image {
      background: #141347;
      overflow: hidden;
      height: 55dvh;
      .common-img {
        width: 50%;
      }
      .fitnessone {
        position: absolute;
        top: 95px;
        left: 0;
        opacity: 1;
      }
      .fitnesssecond {
        position: absolute;
        top: 0%;
        right: 0;
        width: 40%;
      }
    }
    .content {
      background: #0e103a;
      height: 45dvh;
      padding-left: 6%;
      overflow: hidden;

      h1,
      p,
      a {
        margin: 0;
        padding: 0;
        color: #fff;
      }
      h1 {
        margin-bottom: 6px;
        font-size: 16px;
        font-weight: 700;
      }
      p {
        margin-bottom: 30px;
        font-size: 14px;
      }
      button {
        border: 0;
        border-radius: 20px;
        padding: 6px 19px;
        font-size: 14px;
      }
    }
  }

  @media only screen and (min-width: 767px) {
    .carouselwrapper {
      display: grid !important;
      grid-template-columns: 1fr 1fr !important;
      grid-template-rows: 1fr !important;
      direction: rtl !important;
      .content,
      .image {
        height: 100dvh;
      }
    }
    .carouselwrapper .content {
      padding-top: 270px;
    }
    .carouselwrapper .image .fitnessone {
      top: 250px;
    }
  }

  .carouselsecond {
    .image {
      background: #0d121b;

      .pizza3 {
        left: 0;
        bottom: 0;
        width: 40%;
      }
      .pizza7 {
        top: 0;
        left: 0;
        width: 25%;
      }
      .pizza1 {
        left: 50%;
        top: 30%;
        width: 35%;
      }
      .pizza4 {
        bottom: 0;
        right: -70px;
        width: 40%;
      }
      .pizza5 {
        top: -223px;
        right: 50px;
        width: 40%;
      }
      .pizza6 {
        right: -139px;
        top: 30%;
        bottom: 0;
        width: 37%;
      }
      .pizza2 {
        bottom: 0;
        right: 82px;
        width: 32%;
      }
    }
    .content {
      background: #142136;
    }
  }

  .carouselfirst {
    .image {
      .fitneddimgSecond {
        left: 0;
        bottom: 0;
        width: 45%;
      }
    }
  }
  .carouselthird {
    .image {
      background: #53d0eb;

      .thirdcarouseone {
        top: -30px;
        left: 33%;
        width: 30%;
      }
      .thirdcarousetwo {
        left: 0;
        top: -159px;
        width: 30%;
      }
      .thirdcarousethree {
        bottom: -135px;
        left: 240px;
        width: 30%;
      }
      .thirdcarousefour {
        left: 0;
        bottom: -30px;
        width: 30%;
      }
      .thirdcarousefive {
        right: 0;
        top: 15%;
        width: 30%;
      }
    }
    .content {
      background: #135fda;
    }
  }

  .carouselfourth {
    .image {
      background: #05236c;
      .common-img {
        width: 100%;
      }
    }
    .content {
      background: #052889;
    }
  }
  .carouselfive {
    .image {
      background: #57b76a;
      .common-img {
        width: 50%;
      }
      .fifthcarouseone {
        bottom: -65px;
        right: 34px;
        width: 40%;
      }
      .fifthcarousetwo {
        left: 18%;
        bottom: -21px;
        width: 40%;
      }
      .fifthcarousethree {
      }
    }
    .content {
      background: #347344;
    }
  }

  .carouselsix {
    .image {
      background: #1f1b61;

      .sixthcarouseone {
        bottom: 0;
        left: 40px;
        width: 45%;
      }
      .sixthcarousetwo {
        right: 0;
        top: 0;
        width: 45%;
      }
    }
    .content {
      background: #9d43ba;
    }
  }
  .carouselseven {
    .image {
      background: #ceccd0;

      .seventhcarouseone {
        left: 0;
        top: 30%;
        width: 40%;
      }

      .seventhcarousetwo {
        right: 30px;
        top: 20%;
        width: 45%;
      }
      .seventhcarousethree {
        left: 0;
        top: 20%;
        width: 45%;
      }
    }
    .content {
      background: #6437a5;
    }
  }

  .slick-dots {
    display: block;
    position: absolute;
    bottom: 9px;
    li {
      margin: 0;
      padding: 0;
      width: 17px;
      height: 17px;
    }
  }
  .slick-vertical .slick-slide {
    border: 0;
  }

  svg {
    // background-color: transparent !important;
    width: 25%;
    position: absolute;
    top: 12%;
    left: 35%;
    border-radius: 50%;
    border: 0;
  }
`;

function Landing() {
  const [isVertical, setIsVertical] = useState(window.innerWidth > 767);
  const sliderRef = useRef(null);

  const [currentSlide, setCurrentSlide] = useState(0);

  useEffect(() => {
    const handleResize = () => {
      setIsVertical(window.innerWidth > 767);
    };

    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  useEffect(() => {
    const handleWheel = (e) => {
      if (sliderRef.current) {
        const delta = e.deltaY;

        if (delta > 0) {
          sliderRef.current.slickNext();
        } else {
          sliderRef.current.slickPrev();
        }
      }
    };

    window.addEventListener("wheel", handleWheel);

    return () => {
      window.removeEventListener("wheel", handleWheel);
    };
  }, [sliderRef]);

  const ref = useRef([]);

  const pararef = useRef([]);

  pararef.current = [];

  const anchorref = useRef([]);

  anchorref.current = [];

  ref.current = [];

  let carouseoneimgdown = useRef(null);
  let carouseoneimgup = useRef(null);

  let carousetwoimgone = useRef(null);
  let carousetwoimgtwo = useRef(null);
  let carousetwoimgthree = useRef(null);

  let carousetwoimgfour = useRef(null);
  let carousetwoimgfive = useRef(null);
  let carousetwoimgsix = useRef(null);
  let carousetwoimgseven = useRef(null);

  let carouselthreeone = useRef(null);
  let carouselthreetwo = useRef(null);
  let carouselthreethree = useRef(null);
  let carouselthreefour = useRef(null);
  let carouselthreefive = useRef(null);

  let carouselfourthone = useRef(null);

  let carouselfifthone = useRef(null);
  let carouselfifthtwo = useRef(null);
  let carouselfifththree = useRef(null);

  let carouselsixthone = useRef(null);
  let carouselsixthtwo = useRef(null);

  let carouselsevenone = useRef(null);
  let carouselseventwo = useRef(null);
  let carouselseventhree = useRef(null);

  const [visitedSlides, setVisitedSlides] = useState([]);

  const onSlideChange = (slick, currentSlide) => {
    resetDotColors();

    updateDotColor(currentSlide);
  };

  const resetDotColors = () => {
    for (let i = 0; i <= 7; i++) {
      const dot = document.querySelector(`.dotsfill${i}`);
      if (dot) {
        dot.style.fill = "rgb(0, 146, 255)";
      }
    }
  };

  const updateDotColor = (currentSlide) => {
    const dot = document.querySelector(`.dotsfill${currentSlide}`);

    if (dot) {
      dot.style.fill = "red";
    }

    setVisitedSlides((prevVisitedSlides) => [
      ...new Set([...prevVisitedSlides, currentSlide]),
    ]);
  };

  useEffect(() => {
    updateDotColor(0);
  }, []);

  function svgbackgroundcolor(currentSlide) {
    const svgcolor = document.querySelector(".svg-color");
    if (currentSlide === 0) {
      svgcolor.style.backgroundColor = "#0e103a";
    } else if (currentSlide === 1) {
      svgcolor.style.backgroundColor = "#142136";
    } else if (currentSlide === 2) {
      svgcolor.style.backgroundColor = "#135fda";
    } else if (currentSlide === 3) {
      svgcolor.style.backgroundColor = "#052889";
    } else if (currentSlide === 4) {
      svgcolor.style.backgroundColor = "#347344";
    } else if (currentSlide === 5) {
      svgcolor.style.backgroundColor = "#9d43ba";
    } else if (currentSlide === 6) {
      svgcolor.style.backgroundColor = "#6437a5";
    }
  }

  useEffect(() => {
    if (isVertical) {
      onSlideChange(currentSlide);
      svgbackgroundcolor(currentSlide);
      console.log(currentSlide);

      ref.current.forEach((el, index) => {
        gsap.fromTo(
          el,
          { opacity: 0, y: -500, rotateX: 100 },
          {
            opacity: 1,
            y: 0,
            rotateX: 0,
            duration: 0.5,
            ease: "power1.out",
            scrollTrigger: {
              trigger: el,
              start: "top bottom",
              toggleActions: "play none none reverse",
            },
          }
        );
      });

      pararef.current.forEach((el, index) => {
        gsap.fromTo(
          el,
          {
            autoAlpha: 0,
            y: -5,
            rotateX: 100,
          },
          {
            autoAlpha: 1,
            y: 0,
            rotateX: 0,
            duration: 0.8,
            ease: "power1.out",
            delay: 0.2,
          }
        );
      });

      anchorref.current.forEach((el, index) => {
        gsap.fromTo(
          el,
          {
            autoAlpha: 0,
            y: -5,
            rotateX: 100,
          },
          {
            autoAlpha: 1,
            y: 0,
            rotateX: 0,
            duration: 0.8,
            ease: "power1.out",
            delay: 0.3,
          }
        );
      });

      if (currentSlide === 0) {
        gsap.from(carouseoneimgdown, {
          autoAlpha: 1,
          y: 100,
          duration: 0.8,
          ease: "power3.out",
        });
        gsap.from(carouseoneimgup, {
          autoAlpha: 1,
          y: -500,
          duration: 0.8,
          ease: "power3.out",
        });
      } else if (currentSlide === 1) {
        gsap.from(carousetwoimgone, {
          autoAlpha: 0,
          y: 200,
          duration: 0.8,
          ease: "power1.out",
          delay: 0.3,
        });
        gsap.from(carousetwoimgtwo, {
          autoAlpha: 0,
          y: -500,
          duration: 0.8,
          ease: "power1.out",
          delay: 0.3,
        });
        gsap.from(carousetwoimgthree, {
          autoAlpha: 0,
          y: 500,
          duration: 0.8,
          ease: "power1.out",
          delay: 0.3,
        });
        gsap.from(carousetwoimgfour, {
          autoAlpha: 0,
          x: 500,
          duration: 0.8,
          ease: "power1.out",
          delay: 0.3,
        });
        gsap.from(carousetwoimgfive, {
          autoAlpha: 0,
          x: -500,
          duration: 0.8,
          ease: "power1.out",
          delay: 0.3,
        });
        gsap.from(carousetwoimgsix, {
          autoAlpha: 0,
          x: 500,
          duration: 0.8,
          ease: "power1.out",
          delay: 0.3,
        });
        gsap.from(carousetwoimgseven, {
          autoAlpha: 0,
          x: -500,
          duration: 0.8,
          ease: "power1.out",
          delay: 0.3,
        });
      } else if (currentSlide === 2) {
        gsap.from(carouselthreeone, {
          autoAlpha: 0,
          y: -50,
          duration: 0.8,
          ease: "power1.out",
          delay: 0.3,
        });
        gsap.from(carouselthreetwo, {
          autoAlpha: 0,
          y: 100,
          duration: 0.8,
          ease: "power1.out",
          delay: 0.3,
        });
        gsap.from(carouselthreethree, {
          autoAlpha: 0,
          y: 50,
          duration: 0.8,
          ease: "power1.out",
          delay: 0.3,
        });
        gsap.from(carouselthreefour, {
          autoAlpha: 0,
          y: 100,
          duration: 0.8,
          ease: "power1.out",
          delay: 0.3,
        });
        gsap.from(carouselthreefive, {
          autoAlpha: 0,
          y: 500,
          duration: 0.8,
          ease: "power1.out",
          delay: 0.3,
        });
      } else if (currentSlide === 3) {
        gsap.from(carouselfourthone, {
          autoAlpha: 0,
          y: 500,
          duration: 0.5,
          ease: "power4.out",
          delay: 0.3,
        });
      } else if (currentSlide === 4) {
        gsap.from(carouselfifthone, {
          autoAlpha: 0,
          y: 500,
          duration: 0.5,
          ease: "power4.out",
          delay: 0.3,
        });
        gsap.from(carouselfifthtwo, {
          autoAlpha: 0,
          y: 50,
          duration: 1,
          ease: "power4.out",
          delay: 0.3,
        });
        gsap.from(carouselfifththree, {
          autoAlpha: 0,
          y: 500,
          duration: 1,
          ease: "bounce.out",
          delay: 0.3,
        });
      } else if (currentSlide === 5) {
        gsap.from(carouselsixthone, {
          autoAlpha: 0,
          y: 500,
          duration: 0.5,
          ease: "power4.out",
          delay: 0.3,
        });
        gsap.from(carouselsixthtwo, {
          autoAlpha: 0,
          y: -500,
          duration: 0.5,
          ease: "power2.out",
          delay: 0.3,
        });
      } else if (currentSlide === 6) {
        gsap.from(carouselsevenone, {
          autoAlpha: 0,
          x: 100,
          duration: 0.5,
          ease: "bounce.out",
          delay: 0.3,
        });
        gsap.from(carouselseventwo, {
          autoAlpha: 0,
          y: -500,
          duration: 0.5,
          ease: "power2.out",
          delay: 0.3,
        });
        gsap.from(carouselseventhree, {
          autoAlpha: 0,
          y: 500,
          duration: 0.5,
          ease: "power2.out",
          delay: 0.3,
        });
      }
    }
  }, [currentSlide, isVertical]);

  const addtoRefs = (el) => {
    if (el && !ref.current.includes(el)) {
      ref.current.push(el);
    }
  };

  const addparaRefs = (el) => {
    if (el && !pararef.current.includes(el)) {
      pararef.current.push(el);
    }
  };

  const addanchorRefs = (el) => {
    if (el && !anchorref.current.includes(el)) {
      anchorref.current.push(el);
    }
  };

  const handleAfterChange = (currentSlide) => {};

  const sliderSettings = {
    dots: isVertical ? false : true,
    infinite: false,
    arrows: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    vertical: isVertical,
    verticalSwiping: isVertical,
    afterChange: handleAfterChange,
    // beforeChange: onSlideChange,
    beforeChange: (oldIndex, newIndex) => setCurrentSlide(newIndex),
  };
  return (
    <WrapperStyled>
      <Slider {...sliderSettings} ref={sliderRef}>
        <div className="carouselwrapper carouselfirst">
          <div className="image d-flex position-relative">
            <img
              className="common-img position-absolute fitneddimgSecond"
              src={fitneddimgSecond}
              alt=""
              // ref={(el) => (carouseoneimgdown = el)}
              ref={addtoRefs}
            />
            <img
              className="common-img fitnesssecond position-absolute"
              src={fitneddimg}
              alt=""
              // ref={(el) => (carouseoneimgup = el)}
              ref={addtoRefs}
            />
          </div>
          <div className="content  d-flex justify-content-center flex-column">
            <h1 ref={addtoRefs}>
              Welcome to Universal Plumbing Works, where craftsmanship meets
              innovation. We specialize in providing top-notch plumbing,
              interior design, and construction services tailored to meet your
              unique needs.
            </h1>
            {/* <p ref={addparaRefs}>This is sample text for ABC 2</p> */}
            {/* <a href="" ref={addanchorRefs}>
              CASE STUDY- <button>Coming Soon</button>
            </a> */}
          </div>
        </div>
        {/* 2nd carousel */}
        <div className="carouselwrapper carouselsecond">
          <div className="image d-flex position-relative">
            <img
              className="common-img  position-absolute pizza3"
              src={pizza3}
              alt=""
              ref={(el) => (carousetwoimgone = el)}
            />
            <img
              className="common-img position-absolute pizza1"
              src={pizza1}
              alt=""
              ref={(el) => (carousetwoimgtwo = el)}
            />
            <img
              className="common-img position-absolute pizza2"
              src={pizza2}
              alt=""
              ref={(el) => (carousetwoimgthree = el)}
            />
            <img
              className="common-img position-absolute pizza4"
              src={pizza4}
              alt=""
              ref={(el) => (carousetwoimgfour = el)}
            />
            <img
              className="common-img position-absolute pizza5"
              src={pizza5}
              alt=""
              ref={(el) => (carousetwoimgfive = el)}
            />
            <img
              className="common-img position-absolute pizza6"
              src={pizza6}
              alt=""
              ref={(el) => (carousetwoimgsix = el)}
            />
            <img
              className="common-img position-absolute pizza7"
              src={pizza7}
              alt=""
              ref={(el) => (carousetwoimgseven = el)}
            />
          </div>
          <div className="content d-flex justify-content-center flex-column">
            <h1 ref={addtoRefs}>ABC 3</h1>
            <p ref={addparaRefs}>This is sample text for ABC 3</p>
            <a ref={addanchorRefs} href="">
              View Case Study
            </a>
          </div>
        </div>
        {/* 3rd carousel */}
        <div className="carouselwrapper carouselthird">
          <div className="image d-flex position-relative">
            <img
              className="common-img position-absolute thirdcarouseone"
              src={thirdcarouseone}
              alt=""
              ref={(el) => (carouselthreeone = el)}
            />
            <img
              className="common-img position-absolute thirdcarousetwo"
              src={thirdcarousetwo}
              alt=""
              ref={(el) => (carouselthreetwo = el)}
            />
            <img
              className="common-img position-absolute thirdcarousethree"
              src={thirdcarousethree}
              alt=""
              ref={(el) => (carouselthreethree = el)}
            />
            <img
              className="common-img position-absolute thirdcarousefour"
              src={thirdcarousefour}
              alt=""
              ref={(el) => (carouselthreefour = el)}
            />
            <img
              className="common-img position-absolute thirdcarousefive"
              src={thirdcarousefive}
              alt=""
              ref={(el) => (carouselthreefive = el)}
            />
          </div>
          <div className="content d-flex justify-content-center flex-column">
            <h1 ref={addtoRefs}>ABC 4</h1>
            <p ref={addparaRefs}>This is sample text for ABC 4</p>
            <a ref={addanchorRefs} href="">
              View Case Study
            </a>
          </div>
        </div>
        {/* 4th carousel */}
        <div className="carouselwrapper carouselfourth">
          <div className="image d-flex position-relative">
            <img
              className="common-img fitnessone"
              src={fourthcarouseone}
              alt=""
              ref={(el) => (carouselfourthone = el)}
            />
          </div>
          <div className="content d-flex justify-content-center flex-column">
            <h1 ref={addtoRefs}>ABC 4</h1>
            <p ref={addparaRefs}>This is sample text for ABC 4</p>
            <a ref={addanchorRefs} href="">
              View Case Study
            </a>
          </div>
        </div>

        {/* 5th carousel */}
        <div className="carouselwrapper carouselfive">
          <div className="image d-flex position-relative">
            <img
              className="common-img position-absolute fifthcarouseone"
              src={fifthcarouseone}
              alt=""
              ref={(el) => (carouselfifthone = el)}
            />
            <img
              className="common-img position-absolute fifthcarousetwo"
              src={fifthcarousetwo}
              alt=""
              ref={(el) => (carouselfifthtwo = el)}
            />
            <img
              className="common-img position-absolute fifthcarousethree"
              src={fifthcarousethree}
              alt=""
              ref={(el) => (carouselfifththree = el)}
            />
          </div>
          <div className="content d-flex justify-content-center flex-column">
            <h1 ref={addtoRefs}>XYZ 123</h1>
            <p ref={addparaRefs}>This is sample text for XYZ 123</p>
            <a ref={addanchorRefs} href="">
              View Case Study
            </a>
          </div>
        </div>

        {/* 6th carousel */}
        <div className="carouselwrapper carouselsix">
          <div className="image d-flex position-relative">
            <img
              className="common-img position-absolute sixthcarouseone"
              src={sixthcarouseone}
              alt=""
              ref={(el) => (carouselsixthone = el)}
            />
            <img
              className="common-img position-absolute sixthcarousetwo"
              src={sixthcarousetwo}
              alt=""
              ref={(el) => (carouselsixthtwo = el)}
            />
          </div>
          <div className="content d-flex justify-content-center flex-column">
            <h1 ref={addtoRefs}>ABC 1</h1>
            <p ref={addparaRefs}>This is sample text for ABC 1</p>
            <a ref={addanchorRefs} href="">
              View Case Study
            </a>
          </div>
        </div>

        {/* 7th carousel */}
        <div className="carouselwrapper carouselseven">
          <div className="image d-flex position-relative">
            <img
              className="common-img position-absolute seventhcarouseone"
              src={seventhcarouseone}
              alt=""
              ref={(el) => (carouselsevenone = el)}
            />
            <img
              className="common-img position-absolute seventhcarousetwo"
              src={seventhcarousetwo}
              alt=""
              ref={(el) => (carouselseventwo = el)}
            />
            <img
              className="common-img position-absolute seventhcarousethree"
              src={seventhcarousethree}
              alt=""
              ref={(el) => (carouselseventhree = el)}
            />
          </div>
          <div className="content d-flex justify-content-center flex-column">
            <h1 ref={addtoRefs}>ABC 123</h1>
            <p ref={addparaRefs}>This is sample text for ABC 123</p>
            <a ref={addanchorRefs} href="">
              View Case Study
            </a>
          </div>
        </div>
      </Slider>
      {isVertical ? (
        <svg
          version="1.1"
          id="transring"
          xmlns="http://www.w3.org/2000/svg"
          x="0px"
          y="0px"
          viewBox="0 0 414 414"
          className="svg-color"
          style={{
            enableBackground: "new 0 0 414 414",
            backgroundColor: "lightblue",
          }}
        >
          <path
            id="Transparent_Ring"
            className="transrg"
            style={{
              opacity: 0.4,
              fill: "none",
              stroke: "#FFFFFF",
              strokeWidth: 2,
              strokeMiterlimit: 10,
              enableBackground: "new",
            }}
            d="M84.2,85c31.3-31.5,74.7-51,122.7-51c95.5,0,173,77.5,173,173s-77.5,173-173,173 c-47.8,0-91-19.4-122.3-50.7"
          ></path>

          <path
            id="Opaque_Ring"
            className="transrgwht"
            strokeDasharray="0,1000"
            style={{
              fill: "none",
              stroke: "rgb(255, 255, 255)",
              strokeWidth: 2,
              strokeMiterlimit: 10,
              strokeDasharray: "10, 1000",
            }}
            d="M84.2,85c31.3-31.5,74.7-51,122.7-51c95.5,0,173,77.5,173,173s-77.5,173-173,173 c-47.8,0-91-19.4-122.3-50.7"
          ></path>

          <g id="Dots1" className="dots-nav">
            <g>
              <path
                className="dotsst dotsfill1"
                style={{ fill: "rgb(0, 146, 255)", opacity: 1 }}
                d="M84,80 c-3.3,0-6,2.7-6,6s2.7,6,6,6s6-2.7,6 -6 S 87, 80, 84,80 z"
              ></path>
            </g>
            <g>
              <path
                className="dotsstro1"
                style={{
                  opacity: 1,
                  fill: "none",
                  stroke: "rgb(255, 255, 255)",
                  strokeWidth: 2,
                  strokeMiterlimit: 10,
                }}
                d="M84,80c-3.3,0-6,2.7-6,6s2.7,6,6,6s6-2.7,6 -6 S 87, 80, 84,80 z"
              ></path>
            </g>
          </g>
          <g id="Dots2" class="dots-nav">
            <g>
              <path
                class="dotsst dotsfill2"
                style={{ fill: "rgb(0, 146, 255)", opacity: 1 }}
                d="M206.8,29c-3.3,0-6,2.7-6,6s2.7,6,6,6s6-2.7,6 -6S 210.1,29, 206.8,29 z M206.8"
              ></path>
            </g>
            <g>
              <path
                class="dotsstro2"
                style={{
                  opacity: 1,
                  fill: "none",
                  stroke: "rgb(255, 255, 255)",
                  strokeWidth: 2,
                  strokeMiterlimit: 10,
                }}
                d="M206.8,29c-3.3,0-6,2.7-6,6s2.7,6,6,6s6-2.7,6 -6S 210.1,29, 206.8,29 z M206.8"
              ></path>
            </g>
          </g>
          <g id="Dots3" class="dots-nav">
            <g>
              <path
                class="dotsst dotsfill3"
                style={{ fill: "rgb(0, 146, 255)", opacity: 1 }}
                d="M332,85 c-3.3,0-6,2.7-6,6s2.7,6,6,6s6-2.7,6 -6 S 335,85, 332,85 z M332"
              ></path>
            </g>
            <g>
              <path
                class="dotsstro3"
                style={{
                  opacity: 1,
                  fill: "none",
                  stroke: "rgb(255, 255, 255)",
                  strokeWidth: 2,
                  strokeMiterlimit: 10,
                }}
                d="M332,85 c-3.3,0-6,2.7-6,6s2.7,6,6,6s6-2.7,6 -6 S 335,85, 332,85 z M332"
              ></path>
            </g>
          </g>
          <g id="Dots4" class="dots-nav">
            <g>
              <path
                class="dotsst dotsfill4"
                style={{ fill: "rgb(0, 146, 255)", opacity: 1 }}
                d="M380,205 c-3.3,0-6,2.7-6,6s2.7,6,6,6s6-2.7,6 -6 S 383,205, 380,205 z M380"
              ></path>
            </g>
            <g>
              <path
                class="dotsstro4"
                style={{
                  opacity: 1,
                  fill: "none",
                  stroke: "rgb(255, 255, 255)",
                  strokeWidth: 2,
                  strokeMiterlimit: 10,
                }}
                d="M380,205 c-3.3,0-6,2.7-6,6s2.7,6,6,6s6-2.7,6 -6 S 383,205, 380,205 z M380"
              ></path>
            </g>
          </g>
          <g id="Dots5" class="dots-nav">
            <g>
              <path
                class="dotsst dotsfill5"
                style={{ fill: "rgb(0, 146, 255)", opacity: 1 }}
                d="M335,315 c-3.3,0-6,2.7-6,6s2.7,6,6,6s6-2.7,6 -6 S 340,315, 335,315 z M335"
              ></path>
            </g>
            <g>
              <path
                class="dotsstro5"
                style={{
                  opacity: 1,
                  fill: "none",
                  stroke: "rgb(255, 255, 255)",
                  strokeWidth: 2,
                  strokeMiterlimit: 10,
                }}
                d="M335,315 c-3.3,0-6,2.7-6,6s2.7,6,6,6s6-2.7,6 -6 S 340,315, 335,315 z M335"
              ></path>
            </g>
          </g>
          <g id="Dots6" class="dots-nav">
            <g>
              <path
                class="dotsst dotsfill6"
                style={{ fill: "rgb(0, 146, 255)", opacity: 1 }}
                d="M210,375 c-3.3,0-6,2.7-6,6s2.7,6,6,6s6-2.7,6 -6 S 215,375, 210,375 z M210"
              ></path>
            </g>
            <g>
              <path
                class="dotsstro6"
                style={{
                  opacity: 1,
                  fill: "none",
                  stroke: "rgb(255, 255, 255)",
                  strokeWidth: 2,
                  strokeMiterlimit: 10,
                }}
                d="M210,375 c-3.3,0-6,2.7-6,6s2.7,6,6,6s6-2.7,6 -6 S 215,375, 210,375 z M210"
              ></path>
            </g>
          </g>
          <g id="Dots7" class="dots-nav">
            <g>
              <path
                class="dotsst dotsfill7"
                style={{ fill: "rgb(0, 146, 255)", opacity: 1 }}
                d="M88,324 c-3.3,0-6,2.7-6,6s2.7,6,6,6s6-2.7,6 -6 S 92,324, 88,324 z M88"
              ></path>
            </g>
            <g>
              <path
                class="dotsstro7"
                style={{
                  opacity: 1,
                  fill: "none",
                  stroke: "rgb(255, 255, 255)",
                  strokeWidth: 2,
                  strokeMiterlimit: 10,
                }}
                d="M88,324 c-3.3,0-6,2.7-6,6s2.7,6,6,6s6-2.7,6 -6 S 92,324, 88,324 z M88"
              ></path>
            </g>
          </g>
        </svg>
      ) : (
        ""
      )}
    </WrapperStyled>
  );
}

export default Landing;
